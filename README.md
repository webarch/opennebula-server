# OpenNebula Server

Ansible configuration to create a local developement / demo OpenNebula server,
on the local network at `opennebula.lan`, based on the [Single Front-end
Installation
documentation](https://docs.opennebula.io/6.2/installation_and_configuration/frontend_installation/install.html#single-front-end-installation).
