#!/usr/bin/env bash

if [[ "${1}" != "" ]]; then
  ansible-playbook opennebula.yml --tags "${1}" -vv
else
  ansible-playbook opennebula.yml -vv
fi
