#!/usr/bin/env bash

if [[ "${1}" != "" ]]; then
  ansible-playbook -v root.yml --limit "${1}"
else
  ansible-playbook -v root.yml 
fi

